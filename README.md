# README #

* This is an Title tracking Application. The User can search any Title they need.
* Version 1.0


### How do I get set up? ###

* Download all the file from  the Title tracking application on the PC. The PC should have Visual Studio 2013 i,.e 4.5 framework installed.
* Configuration: Add the Connection strings in the App.config as well as Web.Config File.
* Dependencies
* Database configuration: 
<add name="TitlesEntities" connectionString="Data Source=tcp:bx8cna5bk0.database.windows.net,1433;Initial Catalog=Titles;User Id=readuser@bx8cna5bk0;Password=read!234#Q~$;MultipleActiveResultSets=True" ProviderName="System.Data.EntityClient" />

What I have used in the this project?
1) Used AngularJS and Bootstrap as the front End
2) Used MVC 4 as the Pattern to connect the front End with the database layer
3) Used repository pattern to communicate the SQL Server
4) used Entity Frame Work 6 to communicate with the database.
5) used caching , Error Handling and Logging to handle user responsiveness.
6) Used Newtonsoft.Json to send data in the Json Format
7) Used HTML5 for the front end
 8) Used Structure Map as an IOC container to create Instance of the Class and to make it more helpful for TDD(Test Driven Development)
9)Used Sql server 2008 database as the data store.

To load the solution file you need to use the file 
TitleProjectTracking.sln

