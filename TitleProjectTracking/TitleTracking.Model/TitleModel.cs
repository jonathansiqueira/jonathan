﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TitleTracking.Model
{
    public class TitleModel
    {
        public int TitleID { get; set; }
        public int? ReleaseYear { get; set; }      
        public string TitleName { get; set; }       
        public int? AwardYear { get; set; }
        public string Award { get; set; }      
        public string Participants { get; set; }
        public string Language { get; set; }
        public string Description { get; set; }
    }
}
