﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TitleTracking.Library.Exceptions
{
           public class TitleException : System.Exception
        {
            public TitleException()
            { }

            public TitleException(string message, System.Exception innerException)
                : base(message, innerException)
            { }

            public TitleException(string message) : base(message) { }
        }
    
}
