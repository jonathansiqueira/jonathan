﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TitleTracking.Library.Configuration
{
    public class WebConfigApplicationSettings
    {
        /// <summary>
        /// Rule Set Caching Expiration
        /// </summary>
        public string RuleSetAbsoluteExpirationCachePolicy
        {
            get { return ConfigurationManager.AppSettings["CacheMemRuleSetAbsoluteExpirationCachePolicy"]; }
        }
        /// <summary>
        /// User Authorization Expiration
        /// </summary>
        public string UserAuthAbsoluteExpirationCachePolicy
        {
            get { return ConfigurationManager.AppSettings["CacheMemUserAuthAbsoluteExpirationCachePolicy"]; }
        }
    }
}
