﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TitleTracking.Library.Configuration
{
    public class ApplicationSettingsFactory
    {
        private static WebConfigApplicationSettings _applicationSettings;

        public ApplicationSettingsFactory()
        {
            _applicationSettings = new WebConfigApplicationSettings();
        }

        public static WebConfigApplicationSettings GetApplicationSettings()
        {
            return _applicationSettings ?? (_applicationSettings = new WebConfigApplicationSettings());
        }
    }
}
