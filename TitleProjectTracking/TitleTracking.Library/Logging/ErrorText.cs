﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TitleTracking.Library.Logging
{
    public class ErrorText
    {
        public static string HandledError(string Assembly, string methodName, string errorMessage)
        {
            return string.Format("handle Error Assembly:{0},MethodName:{1},Error:{2}", Assembly, methodName, errorMessage);
        }
        public static string HandledRaiseError(string assembly, string methodName)
        {
            return string.Format("Error in Assembly:{0},MethodName:{1}", assembly, methodName);
        }
        public static string SystemError(string assembly, string errorMessage)
        {
            return string.Format("System error Assembly:{0},Error {1}:", assembly, errorMessage);
        }

        public static string UnexpectedError(string assembly, string errorMessage)
        {
            return string.Format("UnexpectedError error Assembly:{0},Error {1}:", assembly, errorMessage);
        }
        public static string NullInstance(string assembly, string methodName, string instanceName)
        {
            return string.Format("Error:{0}, method:{1}: An instance of {2} is required to use this class.", assembly, methodName, instanceName);
        }
        public static string NullInstance(string instanceName)
        {
            return string.Format("Instance Null Name:{0}", instanceName);
        }
        public static string EntityValidation(string message, string fullErrorMessage)
        {
            return string.Concat(message, " The validation errors are: ", fullErrorMessage);
        }

        public static string ParamterNullEmpty(string erroMessage)
        {
            return string.Format("Paramter Blank {0}", erroMessage);
        }
        public static string CountZero(string erroMessage)
        {
            return string.Format("Count Zero: {0}", erroMessage);
        }
    }
}
