﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TitleProjectTracking.DependencyResolution;
using TitleTracking.DAL.SQL.Repository.Interfaces;
using TitleTracking.DAL.SQL;
using TitleTracking.DAL.SQL.Repository;

namespace UnitTestTitleTracking
{
    [TestClass]
    public class UnitTest1
    {
        StructureMapDependencyResolver _structureMapDependencyResolver;
       ISQLTitleTrackingRepository _TitleTrackingRepository = null;       
        public UnitTest1()
        {
            _structureMapDependencyResolver = new StructureMapDependencyResolver(IoC.Initialize());
            _TitleTrackingRepository = _structureMapDependencyResolver.GetInstance<ISQLTitleTrackingRepository>();
        }
        [TestMethod]
        public void TestMethod1()
        {
           var GetTitlesList = _TitleTrackingRepository.GetTitles();
           Assert.IsNotNull(GetTitlesList);
        }
    }
}
