﻿using TitleTracking.DAL.SQL.Repository.Base;
using TitleTracking.DAL.SQL.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using TitleTracking.Model;
using EmployeeTracking.DAL.SQL;

namespace TitleTracking.DAL.SQL.Repository
{
    public class SQLTitleTrackingRepository : BaseWebRepository<TitlesEntities>, ISQLTitleTrackingRepository
    {
        public SQLTitleTrackingRepository()
        {
            var declaringType = MethodBase.GetCurrentMethod().DeclaringType;
            if (declaringType != null)
                base.Assembly = declaringType.FullName;
        }

        public List<TitleModel> GetTitles()
        {
            return base.GetTitleList().ToList();
        }
    }
    
}
