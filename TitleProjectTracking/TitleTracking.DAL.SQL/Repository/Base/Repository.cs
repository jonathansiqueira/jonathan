﻿using TitleTracking.DAL.SQL.Repository.Interfaces;
using TitleTracking.Library.Exceptions;
using TitleTracking.Library.Logging;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using TitleTracking.Model;
using EmployeeTracking.DAL.SQL;

namespace TitleTracking.DAL.SQL.Repository.Base
{
    public class Repository<T> : Disposable, IRepository<T> where T : class
    {
        public string Assembly { get; set; }

        protected DbSet<T> DbSet { get; set; }

        protected DbContext Context { get; set; }

        public Repository(DbContext dbContext)
        {
            var declaringType = MethodBase.GetCurrentMethod().DeclaringType;
            if (declaringType != null)
                Assembly = declaringType.FullName;

            if (dbContext == null)
                throw new ArgumentException(ErrorText.NullInstance(Assembly, MethodBase.GetCurrentMethod().Name, "dbContext"), "dbContext");

            Context = dbContext;
            this.DbSet = DbContext.Set<T>();

        }
        protected DbContext DbContext
        {
            get { return Context; }
        }

        /// <summary>
        /// Get the List of Data from the Database
        /// </summary>
        /// <returns></returns>
        public virtual IQueryable<T> GetAll()
        {
            try
            {               
                return this.DbSet;
            }
            catch (DbEntityValidationException dbEx)
            {
                
                throw new TitleException(getValidationErrors(dbEx), dbEx);
            }
            catch (SqlException sqlEx)
            {                
                throw new TitleException(ErrorText.HandledRaiseError(Assembly, MethodBase.GetCurrentMethod().Name), sqlEx);
            }
            catch (Exception ex)
            {
                throw new TitleException(ErrorText.HandledRaiseError(Assembly, MethodBase.GetCurrentMethod().Name), ex);
            }           
        }

        /// <summary>
        /// Get the List of Data from the Database
        /// </summary>
        /// <returns></returns>
        public List<TitleModel> GetTitleList()
        {
            try
            {
                using (TitlesEntities context = new TitlesEntities())
                {

                    var TitleList = (from ep in context.Titles
                                     join e in context.Awards on ep.TitleId equals e.TitleId
                                     join sl in context.StoryLines on ep.TitleId equals sl.TitleId                                    
                                     join p in context.Participants on e.Id equals p.Id

                                     select new TitleModel
                                     {
                                         TitleID = ep.TitleId,
                                         ReleaseYear = ep.ReleaseYear,
                                         TitleName = ep.TitleName,
                                         AwardYear = e.AwardYear,
                                         Award = e.Award1,
                                         Participants=p.Name,                                      
                                         Description = sl.Description
                                     });
                    return TitleList.ToList();
                }

               
            }
            catch (DbEntityValidationException dbEx)
            {

                throw new TitleException(getValidationErrors(dbEx), dbEx);
            }
            catch (SqlException sqlEx)
            {
                throw new TitleException(ErrorText.HandledRaiseError(Assembly, MethodBase.GetCurrentMethod().Name), sqlEx);
            }
            catch (Exception ex)
            {
                throw new TitleException(ErrorText.HandledRaiseError(Assembly, MethodBase.GetCurrentMethod().Name), ex);
            }
        }

        private string getValidationErrors(DbEntityValidationException dbEx)
        {
            var errorMessages = dbEx.EntityValidationErrors
                                    .SelectMany(x => x.ValidationErrors)
                                    .Select(x => x.ErrorMessage);
            // Join the list to a single string.
            var fullErrorMessage = string.Join("; ", errorMessages);
            // Combine the original exception message with the new one.            
            return ErrorText.EntityValidation(dbEx.Message, fullErrorMessage);
        }
    }
}
