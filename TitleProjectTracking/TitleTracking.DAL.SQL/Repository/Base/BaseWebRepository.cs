﻿using EmployeeTracking.DAL.SQL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TitleTracking.DAL.SQL.Repository.Base
{
    public class BaseWebRepository<T> : Repository<T> where T : class
    {
        public BaseWebRepository()
            : base(new TitlesEntities())
        {
        }
    }      
}

