﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TitleTracking.Model;

namespace TitleTracking.DAL.SQL.Repository.Interfaces
{
    public interface ISQLTitleTrackingRepository
    {
        List<TitleModel> GetTitles();
    }
}
