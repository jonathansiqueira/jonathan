﻿using TitleProjectTracking.DependencyResolution;
using TitleTracking.Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TitleProjectTracking.Controllers
{
    public class HomeController : Controller
    {
        ITitleTrackingService _TitleTrackingService = null;
        StructureMapDependencyResolver _structureMapDependencyResolver;
    
        public HomeController()
        {
            _structureMapDependencyResolver = new StructureMapDependencyResolver(IoC.Initialize());
            _TitleTrackingService = _structureMapDependencyResolver.GetInstance<ITitleTrackingService>();
          
        }
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";
            return View();
        }
       
        public ActionResult GetDataChunk()
        {
            return Json(this._TitleTrackingService.GetTitles(), JsonRequestBehavior.AllowGet);
        }
    }
}
