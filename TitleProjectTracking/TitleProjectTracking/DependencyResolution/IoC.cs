﻿using TitleTracking.DAL.SQL.Repository;
using TitleTracking.DAL.SQL.Repository.Interfaces;
using TitleTracking.Library;
using TitleTracking.Library.Caching;
using StructureMap;
using StructureMap.Graph;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TitleProjectTracking.DependencyResolution
{
    public static class IoC
    {
        public static IContainer Initialize()
        {
            ObjectFactory.Initialize(x =>
            {
                x.Scan(scan =>
                {
                    scan.TheCallingAssembly();
                    scan.WithDefaultConventions();

                    scan.AssembliesFromApplicationBaseDirectory(
                            assembly => assembly.FullName.Contains("DAL"));
                    scan.AssembliesFromApplicationBaseDirectory(
                            assembly => assembly.FullName.Contains("Service"));
                });
                x.For<IStateManager>().Use<MemoryCacheManager>();
               // x.For<ISQLTitleTrackingRepository>().Use<SQLTitleTrackingRepository>();  
            });
            return ObjectFactory.Container;
        }
    }
}