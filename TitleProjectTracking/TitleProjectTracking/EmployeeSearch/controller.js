﻿'use strict'

var myApp = angular.module('myApp', ['ngGrid'])
           .controller('ngGridController', function ($scope, $http) {
               $scope.myData = [];
               $scope.mySelections = [];
               $scope.filterOptions = { filterText: '' };
               $scope.colDefs = [{ field: 'TitleID', displayName: 'Title ID' },
                                  { field: 'TitleName', displayName: 'Title Name' },
                                  { field: 'ReleaseYear', displayName: 'Release Year' },                                 
                                  { field: 'AwardYear', displayName: 'Award Year' },
                                  { field: 'Award', displayName: 'Award Names' },                                 
                                 { field: 'Participants', displayName: 'Participants' },
                                 { field: 'Description', displayName: 'Description' }
                                    
               ];
              

               $scope.titleDetailsGrid = {
                   data: 'myData',
                   enableColumnResize: true,
                   showFilter: true,
                   multiSelect: false,
                   columnDefs: 'colDefs',
                   filterOptions: $scope.filterOptions,
                   selectedItems: $scope.mySelections,
                   showColumnMenu: true,
                   rowTemplate: '<div ng-dblclick="onDoubleClick(row)" ng-style="{\'cursor\': row.cursor, \'z-index\': col.zIndex() }" ng-repeat="col in renderedColumns" ng-class="col.colIndex()" class="ngCell {{col.cellClass}}" ng-cell></div>',
               };

               var getGUID = function () {
                   var guid = sessionStorage.getItem("user_guid");
                   if (typeof guid === 'undefined' || guid === null)
                       return '';
                   return guid;
               }

               var setGUID = function (GUID) {
                   sessionStorage.setItem("user_guid", GUID);
               }

               var getDataChunk = function () {
                   $http({ method: "get", url: "/home/GetDataChunk/" })
                   .success(function (data) {
                       for (var i = 0; i < data.length; i++)
                           $scope.myData.push(data[i]);
                   });
               }

               $scope.$on('ngGridEventData', function () {
                   $scope.titleDetailsGrid.selectRow(0, true);
                   $scope.totalRecords = $scope.myData.length;
               });

               $scope.onDoubleClick = function (row) {
                   var rowData = $scope.mySelections[0].TitleID;
                   alert("TitleID ID: " + rowData)
               }

               var guid = getGUID();
               if (guid === '')
                   getDataChunk();

           });