﻿using TitleTracking.DAL.SQL;
using TitleTracking.DAL.SQL.Repository.Interfaces;
using TitleTracking.Library;
using TitleTracking.Library.Common;
using TitleTracking.Library.Configuration;
using TitleTracking.Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TitleTracking.Model;

namespace TitleTracking.Service
{
    public class TitleTrackingService : ITitleTrackingService
    {
        IStateManager memoryCacheManager;
        ISQLTitleTrackingRepository TitleTrackingRepository;
        public TitleTrackingService(IStateManager stateManager,ISQLTitleTrackingRepository TitleTrackingRepository)
        {
           memoryCacheManager = stateManager;
           this.TitleTrackingRepository = TitleTrackingRepository;
        }
        public List<TitleModel> GetTitles()
        {
            var appMessages = memoryCacheManager.Get<List<TitleModel>>(TitleTrackingConstant.AppMessages);

            if (appMessages != null) return appMessages;
            appMessages = TitleTrackingRepository.GetTitles();
            memoryCacheManager.Set(TitleTrackingConstant.AppMessages, appMessages, Convert.ToInt16(ApplicationSettingsFactory.GetApplicationSettings().UserAuthAbsoluteExpirationCachePolicy));

            return appMessages.Distinct().ToList();
        }

    }
}
