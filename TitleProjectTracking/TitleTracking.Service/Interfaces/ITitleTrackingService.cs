﻿using TitleTracking.DAL.SQL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TitleTracking.Model;

namespace TitleTracking.Service.Interfaces
{
   public interface ITitleTrackingService
    {
       List<TitleModel> GetTitles();
    }
}
